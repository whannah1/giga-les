load "/home/whannah/NCL/custom_functions.ncl"
begin

	ofile = "~/Data/GIGALES/grid.txt"
	
	bsz = 4
	
;=======================================================
; define input lat/lon grid
;=======================================================
	
	lat = tofloat(ispan(0,2048-1,1))*100./111111. + 50./111111.
	lon = tofloat(ispan(0,2048-1,1))*100./111111. + 50./111111.
	
	lat@long_name = "Latitude"
	lon@long_name = "Longitude"
	
	lat@units = "degress north"
	lon@units = "degrees east"
	
	printline()
	printMM(lat)
	printMM(lon)
	printline()
;=======================================================
; Define new grid
;=======================================================	
	
	lat := block_avg(lat,bsz)
	lon := block_avg(lon,bsz)
	
;=======================================================
; Write new grid file
;=======================================================
	lines = new(7,string)
	
	lines( 0) = "gridtype  = lonlat"
	
	lines( 1) = "xsize     = "+dimsizes(lon)
	lines( 2) = "ysize     = "+dimsizes(lat)
	lines( 3) = "xfirst    = "+lon(0)
	lines( 4) = "xinc      = "+(lon(1)-lon(0))
	lines( 5) = "yfirst    = "+lat(0)
	lines( 6) = "yinc      = "+(lat(1)-lat(0))
	
	print(""+lines)
	
	asciiwrite (ofile,lines)
;=======================================================
; write file of levels for vertical interpolation
;=======================================================
	; this doesn't work because 'cdo intlevel' doesn't
	; appear to accept a file for the levels
	
	;ofile = "~/Data/GIGALES/levels.txt"

;=======================================================
;=======================================================
end
